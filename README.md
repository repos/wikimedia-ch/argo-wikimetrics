# Argo Wikimetrics

Argo Wikimetrics is a collection of tools for extracting metrics from the wiki world. The tool is online here:

https://argo.wikimedia.swiss/

## Details

https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/Argo_Wikimetrics

## MySQL tunnels

The tool needs a direct MySQL connection to Wikimedia Cloud Replicas. To do it, we need something like this for each language:

```
ssh -L 13306:itwiki.analytics.db.svc.wikimedia.cloud:3306 <username>@tools-login.wmflabs.org
```

https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/Argo_Wikimetrics#Languages

## Article lists from Wikidata

This module allows to fetch all the URLs of language-specific Wikipedia articles corresponding to a list of Wikidata items resulting from a configurable SPARQL query.

The example configuration that we offer aims to get the list of English, german, French, Italian and Allemanish Wikipedia articles with coordinates inside a rectangle that circumscribes the Switzerland.

Input
-----
```
node run.js
```

To configurate the action of the script, edit the file `config.json`, adjusting the following parameters according to your needs:

* `languages`: list of the language codes of interest (the language code must be the one used in the wikipedia local url, for example English: en.wikipedia.org -> "en" or "EN")
* `filepath`: the path to the working folder
* `query`: the path of the SPARQL query file that will be made to the Wikidata server to extract the list of entities of interest. The query must return in the **first** column the URI of the Wikidata entity of interest; the other columns don't matter

Output
------

In a first step, the script will save a csv file in the filepath/1A directory, named `items.csv`, containing the list of Wikidata entities that satisfy your query, with eventual additional parameters selected by the query written in a comma-separated-values fashion.

In the second step, the script will save a number of files in the filepath/1A directory, named `LANGUAGECODE.csv` containg one url per line of the articles of Wikipedia, in the language identified by LANGUAGECODE, corresponding to the list of Wikidata entities.

# Article lists from category tree

This module allows to fetch all the URLs of language-specific Wikipedia articles navigating the category tree specified.

The example configuration that we offer aims to get the list of English, german, French, Italian and Allemanish Wikipedia articles that have a category under https://www.wikidata.org/wiki/Q1456250 (max 3 hops).

Input
-----
```
node run.js
```

To configurate the action of the script, edit the file `config.json`, adjusting the following parameters according to your needs:

* `languages`: list of the language codes of interest (the language code must be the one used in the wikipedia local url, for example English: www.en.wikipedia.org->"en" or "EN")
* `filepath`: the path to the working folder
* `category`: the Wikidata entity corresponding to the category root of your desired tree
* `maxLevel`: the max level of recursion inside the category tree
* `databaseConfig`: the path to the file with the database settings

Output
------

The script will save a number of files in the filepath/1B directory, named `LANGUAGECODE.csv` containg one url per line of the articles of Wikipedia, in the language identified by LANGUAGECODE.

# Relevant contributors from list of articles

This module allows to retrieve the list of the most active users regarding to a list of wikipedia articles in different languages.

Input
-----
```
node run.js
```

To configurate the action of the script, edit the file *config.json*, adjusting the following parameters according to your needs:

* `databaseconfig`: the path+filename to a json file containing the fields "user","password","host" for the connection to the wikipedia database
* `filepath`: the path of the working folder.
* `languages`: list of the language codes of interest (the language code must be the one used in the wikipedia local url, for example English: www.en.wikipedia.org->"en" or "EN").
* `continueFromModule`: The module after this one is run. Default C, can be A or B
* `minEditNumber`: Users with less than this number of edits on the pages of interest will be filtered out
* `minEditSize`: Users without at least one additive edit of the this size will be filtered out (probably patrollers)
* `oldestAcceptedEdit`:  Edits older than this timestamp won’t be counted (restrict to the users engaged in the topic in recent times)
* `latestActivity`: Users without an edit after this timestamp will be filtered out

Output
------
The script will save in  the folder filepath/2 a file named "users.csv" the list of the most active users containing, in a comma separated values fashion, the name of the user, the number of edits across the articles of interest, the size of the biggest edit, the latest edit over the articles.

## Contributors interest in article list

This module allows to, provided a list of users, to rank and classify them according to their activity regarding a list of wikipedia articles.

Input
-----
```
node run.js
```

To configurate the action of the script, edit the file *config.json*, adjusting the following parameters according to your needs:

* `blackPages`: an array of optional (can be blank) filenames of csv files containing a list of wikipedia articles that the desidered class of user should not have edited (an edit on them give a penality to the user's rank)
* `databaseconfig`: the path+filename to a json file containing the fields "user","password","host" for the connection to the wikipedia database
* `filepath`: the path, relative to the script folder, of the working folder, containing the input and output files.
* `languages`: list of the language codes of interest (the language code must be the one used in the wikipedia local url, for example English: www.en.wikipedia.org->"en" or "EN"). 

Output
------
The script will save in the folder filepath/3 a file named users.csv containing various stats in csv format for each user describing his activity.

## Report Bug / Propose Feature

https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/Argo_Wikimetrics#bugs

## License

Copyright 2017, 2018, 2019, 2020 Synapta (Loredana Maran, Diego Monti, Dario Salza, Alessio Melandri)

Copyright 2022 Valerio Bozzolan, contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
