const express = require('express');
const path = require('path');
const api = require('./api.js');
const auth = require('http-auth');
const config = require('./config/config.js');
const authRouter = express.Router();
const jobsRouter = express.Router();

module.exports = function (app) {
  /**
   * Create admin authentication
   * @route GET /admin
   * @returns {object} 200 - Admin authentacation
   * @returns {Error}  default - Unexpected error
   */
  function createAuth (req, res, next) {
    const authBasic = auth.basic({
      realm: config.admin.realm
    }, (username, password, nextOperation) => {
      nextOperation(username === config.admin.username && password === config.admin.password);
    }
    );
    (auth.connect(authBasic))(req, res, next);
  }

  /**
   * Create user authentication
   * @route GET /user
   * @returns {object} 200 - user authentacation
   * @returns {Error}  default - Unexpected error
   */
  function createUsersAuth (req, res, next) {
    let validUser = false;
    const authBasic = auth.basic({
      realm: config.usersRealm
    }, (username, password, nextOperation) => {
      config.users.forEach(user => {
        if (validUser) {
          return;
        }
        if (username === user.username && password === user.password) {
          validUser = true;
          nextOperation(true);
        }
      });
      if (!validUser) {
        nextOperation(false);
      }
    });
    (auth.connect(authBasic))(req, res, next);
  }

  app.use('/', express.static(path.join(__dirname, 'pages')));

  app.get('/404', function (req, res) {
    res.sendStatus(404);
  });

  app.get('/500', function (req, res) {
    res.sendStatus(500);
  });

  authRouter.get('/admin', createAuth);
  authRouter.get('/admin/auth', res => res.sendStatus(200));

  authRouter.get('/users', createUsersAuth);
  authRouter.get('/users/auth', res => res.sendStatus(200));

  jobsRouter.route('/')
    .get(api.getJobs)
    .post(api.createJob)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id')
    .get(api.getJob)
    .post(api.createJob)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/users')
    .get(api.getJobUsers)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/users/:userid')
    .get(api.getUser)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/typeStats')
    .get(api.getJobTypeStats)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/typeStats/:userid')
    .get(api.getJobStatsUserType)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/stats')
    .get(api.getJobStats)
    .all(res => res.sendStatus(405));

  jobsRouter.route('/:id/stats/:userid')
    .get(api.getJobStatsUser)
    .all(res => res.sendStatus(405));

  app.use('/api/', authRouter);
  app.use('/api/jobs', jobsRouter);

  // VIEWS
  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/pages/index.html'));
  });

  app.get('/configJob/:id?', (req, res) => {
    res.sendFile(path.join(__dirname, '/pages/views/createJob.html'));
  });

  app.get('/jobs/:id?', (req, res) => {
    if (req.params.id) {
      res.sendFile(path.join(__dirname, '/pages/views/statsJob.html'));
    } else {
      res.sendFile(path.join(__dirname, '/pages/views/jobs.html'));
    }
  });

  app.get('/jobs/:id/users/:user?', (req, res) => {
    if (req.params.id) {
      if (req.params.user) {
        res.sendFile(path.join(__dirname, '/pages/views/statsUserJob.html'));
      } else {
        res.status(404).send('Pagina non trovata');
      }
    } else {
      res.status(404).send('Statistica non trovata');
    }
  });

  // NOT FOUND
  app.get('*', function (req, res) {
    res.status(404).send('Pagina non trovata');
  });
};
