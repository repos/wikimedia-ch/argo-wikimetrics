const got = require('got');
const querystring = require('querystring');
const { Article, Edit } = require('../database');

const TYPES = [
    { id: "place", q: "Q2221906" },
    { id: "person", q: "Q5" },
    { id: "work", q: "Q386724" },
    { id: "event", q: "Q1656682" },
    { id: "organization", q: "Q43229" }];

async function wikiner(entity, type) {
    let query = `
    PREFIX  wd:   <http://www.wikidata.org/entity/>
    PREFIX  wdt:  <http://www.wikidata.org/prop/direct/>

    SELECT  ?s
    WHERE
        {
            {
                ?s wdt:P279 ?instance
                VALUES ?s { wd:${entity} }
                ?instance wdt:P279* wd:${type}
            }
        UNION
            {
                ?s wdt:P31 ?instance
                VALUES ?s { wd:${entity} }
                ?instance wdt:P279* wd:${type}
            }
        }
    `;

    const res = await got.post(process.env.BLAZEGRAPH + '/blazegraph/namespace/kb/sparql', {
        body: querystring.stringify({ query: query }),
        headers: {
            'Accept': 'application/sparql-results+json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    if (JSON.parse(res.body).results.bindings.length) {
        return Promise.resolve(true);
    } else {
        return Promise.resolve(false);
    }
}

module.exports = async (job) => {
    const articles = await Article.findAll({
        where: {
            job_id: job.job_id,
        },
        include: [{
            model: Edit,
            required: true
        }]
    });

    let counter = 1;

    for (const article of articles) {
        console.log("Article " + counter++ + " of " + articles.length);
        for (const type of TYPES) {
            try {
                const result = await wikiner(article.entity, type.q);
                if (result) {
                    await article.update({ type: type.id });
                    break;
                }
            } catch (e) {
                console.error(e);
            }
        }
        if (!article.type) {
            await article.update({ type: "concept" });
        }
    }

    return Promise.resolve();
}