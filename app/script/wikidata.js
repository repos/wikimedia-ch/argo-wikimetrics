const got = require('got');
const neatCsv = require('neat-csv');
const { Article } = require('../database');

let items;

async function load(job) {
    console.log("Requesting list of items via SPARQL query to Wikidata...");

    var sparqlQuery = job.config.query;
    // console.log(sparqlQuery);

    var query = "https://query.wikidata.org/sparql?query=" + encodeURI(sparqlQuery);
    var options = {
        url: query,
        headers: {
            'Accept': "text/csv",
            'User-Agent': "Argo/1.0 (https://synapta.it/; info@synapta.it)"
        }
    }

    const response = await got(options);
    items = await neatCsv(response.body);

    console.log("Completed!");
    console.log("===========================================");
    console.log("Expanding items by languages:");
    return loadAll(job);
}

async function recursiveAsker(items, packnum, properties) {
    let offset = packnum * properties.CONST_LINEGROUP;
    let index = offset;
    let RQ = "";

    console.log("Loading pack " + packnum + "...");

    while (index - offset < properties.CONST_LINEGROUP && index < items.length) {
        RQ += "%3C" + items[index].place + "%3E%20";
        index++;
    }

    let QUERY = properties.CONST_QUERY_P1 + RQ + properties.CONST_QUERY_P2;
    if (RQ != "") {
        await executeRequest(QUERY, properties);

        if (index - offset == properties.CONST_LINEGROUP) {
            await recursiveAsker(items, packnum + 1, properties);
        } else {
            console.log("Completed!");
        }
    } else {
        console.log("Completed!");
    }

    return Promise.resolve();
}

async function executeRequest(QUERY, properties) {
    const options = {
        url: "https://query.wikidata.org/sparql",
        headers: {
            'Accept': "text/csv",
            'Content-Type': "application/x-www-form-urlencoded",
            'User-Agent': "Argo/1.0 (https://synapta.it/; info@synapta.it)"
        },
        body: "query=" + QUERY
    }

    const request = await got.post(options);

    lines = request.body.split("\n");
    let i = 1; // drop header

    while (i < lines.length) {
        if (lines[i].length > 4) {
            let wp = lines[i].split(",")[0];
            let article = decodeURI(wp);
            let language = wp.split("//")[1].split(".")[0];
            let entity = lines[i].split(",")[1].split("/")[4];
            try {
                await Article.create({ job_id: properties.JOB_ID, article: article, language: language, entity: entity, source: 'wikidata' });
            } catch (err) {
                console.log('Duplicate: ', lines[i]);
            }
        }
        i++;
    }

    return Promise.resolve();
}

async function loadAll(job) {
    const properties = {
        CONST_LINEGROUP: 10000,
        CONST_QUERY_P1: encodeURI("SELECT ?wp ?entity WHERE { VALUES ?entity {"),
        CONST_QUERY_P2: encodeURI("} ?wp schema:about ?entity FILTER (regex(STR(?wp), \"LANGS\" ))}"),
        CONST_LANG_P1: "(https://",
        CONST_LANG_P2: ".wikipedia)", // usare | tra campi
        JOB_ID: job.job_id
    };

    CONST_QUERY_P2 = properties.CONST_QUERY_P2.replace(/%22/g, "\"");

    let LANGSTRING = "";
    for (let i = 0; i < job.config.languages.length; i++) {
        LANGSTRING += (properties.CONST_LANG_P1 + job.config.languages[i].toLowerCase() + properties.CONST_LANG_P2);
        if (i != (job.config.languages.length - 1)) {
            LANGSTRING += "|";
        }
    }
    properties.CONST_QUERY_P2 = properties.CONST_QUERY_P2.replace("LANGS", LANGSTRING);

    return recursiveAsker(items, 0, properties);
}

module.exports = async (job) => {
    console.log("Getting articles from Wikidata...");
    console.log("===========================================");
    return load(job);
};
