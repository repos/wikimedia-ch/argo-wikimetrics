const hashmap = require('hashmap');
const wmlabs = require('../wmlabs');
const { Article, User } = require('../database');

const MAPPING_LINES = 200;

let userProcessor;

function escapeSlashes(str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

async function langLoop(job, langIndex) {
    if (job.config.languages.length > langIndex) {
        const language = job.config.languages[langIndex].toLowerCase();
        const dbString = language + "wiki_p";
        console.log("===========================================");
        console.log("Connecting to database " + dbString + " and requesting for packages...");
        if (langIndex > 0) {
            await wmlabs.end();
            packnum = 0;
        }
        await wmlabs.connect(dbString);
        const articles = await Article.findAll({
            where: {
                job_id: job.job_id,
                language: language
            }
        });
        await mappingExecute(job, articles, 0);
        return langLoop(job, langIndex + 1);
    } else {
        return wmlabs.end();
    }
}

async function mappingExecute(job, articles, packnum) {
    let offset = packnum * MAPPING_LINES;
    let index = offset;
    let flag = true;
    let RQ = "";

    console.log("Loading pack " + packnum + "...");

    while (index - offset < MAPPING_LINES && index < articles.length) {
        if (index - offset != 0 && flag) {
            RQ += ",";
        } else {
            flag = true;
        }
        let lineascii = articles[index].article.toString('utf8');
        try {
            RQ += ("\"" + escapeSlashes(lineascii.split("wiki/")[1]) + "\"");
        } catch (err) {
            flag = false;
        }
        index++;
    }

    if (RQ != "") {
        // Get page_id
        const queryX = createQueryOne(RQ);
        try {
            const rows = await wmlabs.query(queryX);
            await userMappingExecution(job, rows);
        } catch(e) {
            // Skip
            console.error(queryX);
        }

        if (index - offset == MAPPING_LINES) {
            await mappingExecute(job, articles, packnum + 1);
        } else {
            console.log("Completed!");
        }
    } else {
        console.log("Completed!");
    }

    return Promise.resolve();
}

async function userMappingExecution(job, pages) {
    let RQ = pages[0].page_id;
    for (let i = 1; i < pages.length; i++)
        RQ += (", " + pages[i].page_id);

    // Get edits
    const queryX = createQueryTwo(job, RQ);
    try {
        const rows = await wmlabs.query(queryX);
        await insertUsersInProcessor(job, rows);
    } catch (e) {
        // Skip
        console.error(queryX);
    }
    return Promise.resolve();
}

async function insertUsersInProcessor(job, rows) {
    let UD;
    let elen = 0;
    for (let i = 1; i < rows.length; i++) {
        key = rows[i].rev_user_text.toString();
        if ((UD = userProcessor.get(key)) == null) {
            UD = {};
            UD.edits = 0;
            UD.maxEdit = 0;
            UD.name = rows[i].rev_user_text.toString();
        }
        UD.edits += 1;
        elen = rows[i].rev_len - rows[i].old_len;
        if (UD.maxEdit < elen) {
            UD.maxEdit = elen;
        }
        if (elen > job.config.minEditSize && (UD.newestEdit == null || UD.newestEdit < rows[i].rev_timestamp.toString())) {
            UD.newestEdit = rows[i].rev_timestamp.toString();
            userProcessor.set(key, UD);
        }
    }
    return Promise.resolve()
}

function createQueryOne(RQ) {
    return `select page_id from page where page_title in (${RQ}) and page_namespace = 0;`;
}

function createQueryTwo(job, RQ) {
    return `select au.actor_user as rev_user, au.actor_name as rev_user_text, t0.rev_len, t0.rev_timestamp, ifnull(t1.rev_len,0) 
    as old_len from revision t0, revision t1, actor_user au
    where t0.rev_actor = au.actor_id and t0.rev_page in(${RQ}) and t0.rev_parent_id=t1.rev_id and au.actor_user!=0 and t0.rev_minor_edit!=1 and t0.rev_timestamp>='${job.config.oldestAcceptedEdit}';`;
}

module.exports = async (job) => {
    console.log("Getting relevant contributors from articles...");

    userProcessor = new hashmap();

    await langLoop(job, 0);

    // Save results
    let users = userProcessor.values();
    users.sort(function (a, b) { return -a.edits + b.edits });
    for (let index = 0; index < users.length; index++) {
        const DU = users[index];
        if (DU.edits > job.config.minEditNumber && DU.maxEdit > job.config.minEditSize && !DU.name.toLowerCase().includes("bot") && DU.newestEdit >= job.config.latestActivity) {
            await User.create({
                job_id: job.job_id,
                username: DU.name
            });
        }
    }

    console.log("Process fully completed!");
    return Promise.resolve();
};