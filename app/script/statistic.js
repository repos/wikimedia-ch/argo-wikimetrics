const hashmap = require('hashmap');
const hashset = require('hashset');
const wmlabs = require('../wmlabs');
const { Article, User, Counter, Edit } = require('../database');

const USER_PER_QUERY = 1;

let userProcessor;
let whitePages;
let blackPages;
let usersToAnalyze;
let totpacks;

async function init(job) {
    console.log("Loading users and pages of interest...");

    // load white pages
    const articles = await Article.findAll({
        where: {
            job_id: job.job_id
        }
    });

    for (let i = 0; i < articles.length; i++) {
        whitePages.add(articles[i].article);
    }

    // load black pages
    for (let i = 0; i < job.config.blackPages.length; i++) {
        blackPages.add(job.config.blackPages[i]);
    }

    // load users to inspect
    const users = await User.findAll({
        where: {
            job_id: job.job_id
        }
    });

    usersToAnalyze = [];

    for (let i = 0; i < users.length; i++) {
        usersToAnalyze[i] = users[i].username;
    }

    usersToAnalyze = shuffleArray(usersToAnalyze);
    totpacks = Math.ceil(usersToAnalyze.length / USER_PER_QUERY);
    console.log("Completed!");
    return openLanguage(job, 0);
}

async function openLanguage(job, languageIndex) {
    if (languageIndex > 0) {
        console.log("Closing database");
        await wmlabs.end();
    }

    if (languageIndex == job.config.languages.length) {
        console.log("Process completed");
        return Promise.resolve();
    }

    const dbString = job.config.languages[languageIndex].toLowerCase() + "wiki_p";
    console.log("===========================================");
    console.log("Connecting to database " + dbString + " (" + (languageIndex + 1) + " of " + job.config.languages.length + ") and requesting for packages...");
    await wmlabs.connect(dbString);
    return preparePack(job, languageIndex, 0, dbString);
}

async function preparePack(job, languageIndex, packnum, database) {
    let offset = packnum * USER_PER_QUERY;
    let index = offset;
    let inEvaluation = [];
    let RQ = "";

    while (index - offset < USER_PER_QUERY && index < usersToAnalyze.length) {
        if (index - offset != 0) {
            RQ += ",";
        }
        inEvaluation[index - offset] = usersToAnalyze[index];
        RQ += ("'" + usersToAnalyze[index].replace(/'/g, "''") + "'"); // single quotes escaped in sql fashion
        index++;
    }

    if (index == offset) {
        return openLanguage(job, languageIndex + 1);
    }

    return sendPack(job, RQ, inEvaluation, languageIndex, packnum, database);
}

async function sendPack(job, RQ, inEvaluation, languageIndex, packnum, database) {
    console.log("Requesting pack " + (packnum + 1) + " of " + totpacks + "...");

    const queryX = queryComposeOne(RQ);
    try {
        const rowsX = await wmlabs.query(queryX, database);
        await openRows(job, rowsX, languageIndex);
    } catch (e) {
        // Skip
        console.error(queryX);
    }


    const queryY = queryComposeTwo(RQ);
    try {
        const rowsY = await wmlabs.query(queryY);
        await openRows(job, rowsY, languageIndex);
    } catch (e) {
        // Skip
        console.error(queryY);
    }

    await collapse(inEvaluation);
    return preparePack(job, languageIndex, packnum + 1);
}

async function openRows(job, rows, languageIndex) {
    for (let i = 0; i < rows.length; i++) {
        let key = rows[i].rev_user_text.toString();
        let UD = userProcessor.get(key);

        if (UD == null) {
            UD = {};
            UD.user = rows[i].rev_user_text.toString().replace(/^'/, "").replace(/'$/, "").replace(/'/g, "''");

            UD.allEdits = { total: 0 };
            UD.whiteEdits = { total: 0 };
            UD.blackEdits = { total: 0 };

            UD.blackReverted = { total: 0 };
            UD.whiteReverted = { total: 0 };
            UD.allReverted = { total: 0 };

            UD.totalCreatedPages = { total: 0 };
            UD.whiteDistinctPagesCreated = { total: 0 };
            UD.whiteDistinctPagesEdited = { total: 0 };
            UD.whiteEditsMediumLengthPerPage = { total: 0 };

            UD.numberOfLanguages = 0;

            UD.articles = null;
        }

        if (UD.articles == null) {
            UD.articles = new hashmap();
        }

        UD = await givePoints(job, UD, rows[i], languageIndex);
        userProcessor.set(key, UD);
    }

    return Promise.resolve();
}

async function collapse(inEvaluation) {
    let i = 0;

    while (i < inEvaluation.length) {
        key = inEvaluation[i];
        UD = userProcessor.get(key);

        let k = 0;
        let sum = 0;
        if (UD == null) {
            i++;
            continue;
        }

        let HT = UD.articles;
        if (HT == null) {
            i++;
            continue;
        }

        let tab = HT.values();
        let prec = 0;
        while (k < tab.length) {
            sum += tab[k].sum;
            k++;
        }

        if (k > 0) {
            prec = UD.whiteEditsMediumLengthPerPage.total * UD.whiteDistinctPagesEdited.total;

            UD.whiteEditsMediumLengthPerPage.total = (prec + sum) / (UD.whiteDistinctPagesEdited.total + k);
            UD.numberOfLanguages++;
            UD.whiteDistinctPagesEdited.total += k;
        }
        UD.articles.clear();
        UD.articles = null;
        i++;
    }

    return Promise.resolve();
}

async function incrementCounter(counter, language) {
    counter.total++;
    if (counter[language] == undefined) {
        counter[language] = 1;
    } else {
        counter[language]++;
    }
    return Promise.resolve();
}

async function givePoints(job, UD, row, languageIndex) {
    const currentPageKey = row.page_title.toString();
    const currentLanguage = job.config.languages[languageIndex].toLowerCase();
    const fwiki = "https://" + currentLanguage + ".wikipedia.org/wiki/";
    const currentArticle = fwiki + currentPageKey;

    if (row.rev_len == row.old_len2) {
        // possibile revert
        if (whitePages.contains(currentArticle)) {
            await incrementCounter(UD.whiteReverted, currentLanguage);
        } else if (blackPages.contains(currentPageKey)) {
            await incrementCounter(UD.blackReverted, currentLanguage);
        }
        await incrementCounter(UD.allReverted, currentLanguage);
    } else if (row.old_len == -1) {
        // is new
        await incrementCounter(UD.totalCreatedPages, currentLanguage);
        if (whitePages.contains(currentArticle)) {
            await incrementCounter(UD.whiteDistinctPagesCreated, currentLanguage);
            if (UD.lastWhiteEdit == null || row.rev_timestamp.toString() > UD.lastWhiteEdit)
                UD.lastWhiteEdit = row.rev_timestamp.toString();
        }
    } else {
        // is simple edit
        if (whitePages.contains(currentArticle)) {
            await incrementCounter(UD.whiteEdits, currentLanguage);
            let uKey = currentPageKey + currentLanguage;
            let stats = UD.articles.get(uKey);
            if (stats == null) {
                stats = {};
                stats.sum = 0;
                stats.edit = 0;
            }
            let len = row.rev_len - row.old_len;
            if (len > 0) {
                stats.sum += len;
                stats.edit++;
            }
            if (UD.lastWhiteEdit == null || row.rev_timestamp.toString() > UD.lastWhiteEdit) {
                UD.lastWhiteEdit = row.rev_timestamp.toString();
            }
            UD.articles.set(uKey, stats);
        } else if (blackPages.contains(currentPageKey)) {
            await incrementCounter(UD.blackEdits, currentLanguage);
        }
    }
    await incrementCounter(UD.allEdits, currentLanguage);
    if (UD.firstEdit == null || row.rev_timestamp.toString() < UD.firstEdit)
        UD.firstEdit = row.rev_timestamp.toString();

    if (whitePages.contains(currentArticle)) {
        const user = await User.findOne({
            where: {
                job_id: job.job_id,
                username: UD.user
            }
        });
        const article = await Article.findOne({
            where: {
                job_id: job.job_id,
                article: currentArticle
            }
        });
        if (user != null && article != null) {
            await Edit.create({ user_id: user.user_id, article_id: article.article_id, timestamp: row.rev_timestamp.toString() });
        }
    }

    return Promise.resolve(UD);
}

function queryComposeOne(RQ) {
    return `select t3.page_title, t0.rev_len, au.actor_name as rev_user_text, t0.rev_timestamp, ifnull(t1.rev_len, 0) as old_len, ifnull(t2.rev_len, 0) as old_len2
    from revision_userindex t0, revision t1, revision t2, page t3, actor_user au
	where t0.rev_actor = au.actor_id
	and t0.rev_page = t3.page_id
    and t0.rev_parent_id = t1.rev_id
    and t1.rev_parent_id = t2.rev_id
    and t3.page_namespace = 0
    and t0.rev_minor_edit != 1
    and au.actor_user in (select user_id from user where user_name in (${RQ}))`;
}

function queryComposeTwo(RQ) {
    return `select t3.page_title, t0.rev_len, au.actor_name as rev_user_text, t0.rev_timestamp, ifnull(t1.rev_len, 0) as old_len, 0 as old_len2
    from revision_userindex t0, revision t1, page t3, actor_user au
	where t0.rev_actor = au.actor_id
	and t0.rev_page = t3.page_id
    and t0.rev_parent_id = t1.rev_id
    and t1.rev_parent_id = 0
    and t3.page_namespace = 0
    and t0.rev_minor_edit != 1
    and au.actor_user in (select user_id from user where user_name in (${RQ}))
    union
    select t3.page_title, t0.rev_len, au.actor_name as rev_user_text, t0.rev_timestamp, -1 as old_len, 0 as old_len2
    from revision_userindex t0, page t3, actor_user au
	where t0.rev_actor = au.actor_id
	and t0.rev_page = t3.page_id
    and t0.rev_parent_id = 0
    and t3.page_namespace = 0
    and au.actor_user in (select user_id from user where user_name in (${RQ}))`;
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

async function saveCounter(obj, type, user) {
    for (let index in obj) {
        await Counter.create({
            job_id: user.job_id,
            user_id: user.user_id,
            type: type,
            language: index,
            value: Math.round(obj[index])
        })
    }
    return Promise.resolve();
}

module.exports = async (job) => {
    console.log("Getting contributors with interest in articles...");

    userProcessor = new hashmap();
    whitePages = new hashset();
    blackPages = new hashset();

    await init(job);

    // Save results
    let statistics = userProcessor.values();
    for (let i = 0; i < statistics.length; i++) {
        const stat = statistics[i];
        const user = await User.findOne({
            where: {
                job_id: job.job_id,
                username: stat.user
            }
        });
        if (user == null) {
            console.error('Unknown user ' + stat.user);
            continue;
        }
        await user.update({
            languages: stat.numberOfLanguages,
            last_white_edit: stat.lastWhiteEdit,
            first_all_edit: stat.firstEdit
        });
        await saveCounter(stat.allEdits, 'all_edits', user);
        await saveCounter(stat.whiteEdits, 'white_edits', user);
        await saveCounter(stat.blackEdits, 'black_edits', user);
        await saveCounter(stat.blackReverted, 'black_reverted', user);
        await saveCounter(stat.whiteReverted, 'white_reverted', user);
        await saveCounter(stat.allReverted, 'all_reverted', user);
        await saveCounter(stat.totalCreatedPages, 'total_created_pages', user);
        await saveCounter(stat.whiteDistinctPagesCreated, 'white_created_pages', user);
        await saveCounter(stat.whiteDistinctPagesEdited, 'white_edited_pages', user);
        await saveCounter(stat.whiteEditsMediumLengthPerPage, 'white_edits_length', user);
    }

    console.log("Process fully completed!");
    return Promise.resolve();
};
