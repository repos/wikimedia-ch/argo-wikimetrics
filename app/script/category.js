const got = require('got');
const wmlabs = require('../wmlabs');
const { Article } = require('../database');

const CONST_ITEMS_PER_QUERY = 25;

let categories;
let wikiIndex;
let hop;
let pages;
let pagesIndex;
let presentLevel;
let nextLevel;
let presentIndex;
let nextIndex;

function createMappingQuery(entity, langs) {
    let langstring = "";
    for (let i = 0; i < langs.length; i++) {
        if (i > 0)
            langstring += "|";
        langstring += ("(https://" + langs[i].toLowerCase() + ".wikipedia)");
    }
    return `SELECT ?wp WHERE 
    {
    <http://www.wikidata.org/entity/${entity}> wdt:P910 ?item .
    ?wp schema:about ?item .
    FILTER (regex(STR(?wp), "${langstring}"))
    }`;
}

async function mappingStep(job) {
    console.log("Mapping category to local names via SPARQL query to Wikidata...");
    const sparqlQuery = createMappingQuery(job.config.category, job.config.languages);
    const query = "https://query.wikidata.org/sparql?query=" + encodeURI(sparqlQuery);
    const options = {
        url: query,
        headers: {
            'Accept': "text/csv",
            'User-Agent': "Argo/1.0 (https://synapta.it/; info@synapta.it)"
        }
    }

    const response = await got(options);
    return extractCategoryNames(job, response.body);
}

async function extractCategoryNames(job, body) {
    const lines = body.split('\n');
    categories = [];
    wikiIndex = -1;
    let outString = "";
    for (let i = 1; i < lines.length && lines[i].length > 3; i++) // first line contains headers, may contain dirty \n at the end
    {
        categories[i - 1] = {};
        categories[i - 1].value = lines[i].split('wiki/')[1].replace('\r', '').split(":");
        categories[i - 1].wiki = lines[i].split('//')[1].split(".")[0];
        categories[i - 1].value = decodeURI(categories[i - 1].value[categories[i - 1].value.length - 1]);
        outString += (categories[i - 1].value + " (" + categories[i - 1].wiki + ") ");
    }
    console.log("Completed!\nCorresponding names are: " + outString);

    return wikiOpen(job);
}

async function wikiOpen(job) {
    wikiIndex++;
    hop = -1;

    if (wikiIndex == categories.length) {
        console.log("Process fully completed!")
        return Promise.resolve();
    }
    pages = [];
    pagesIndex = 0;
    nextLevel = [];
    nextLevel[0] = categories[wikiIndex].value;
    console.log("===========================================");
    console.log("Opening wiki " + categories[wikiIndex].wiki.toUpperCase());
    await wmlabs.connect(categories[wikiIndex].wiki + "wiki_p");
    return doHop(job);
}

async function doHop(job) {
    hop++;
    if (hop == job.config.maxLevel) {
        // Close the connection here to avoid socket inactivity
        await wmlabs.end();
        return getPages(job);
    }
    console.log("Hop " + hop);
    presentLevel = nextLevel;
    nextLevel = [];
    presentIndex = 0;
    nextIndex = 0;
    return getLevelChilds(job);
}

async function getLevelChilds(job) {
    if (presentIndex >= presentLevel.length) {
        return doHop(job);
    }
    console.log("At " + presentIndex + " of " + presentLevel.length);
    const query = buildCategoryQuery(presentLevel, presentIndex, presentIndex + CONST_ITEMS_PER_QUERY);
    presentIndex += CONST_ITEMS_PER_QUERY;
    const rows = await wmlabs.query(query);
    for (let k = 0; k < rows.length; k++) {
        if (rows[k].page_namespace == 14) {
            nextLevel[nextIndex] = rows[k].page_title.toString();
            nextIndex++;
        }
        else if (rows[k].page_namespace == 0) {
            pages[pagesIndex] = [rows[k].page_title.toString(), rows[k].pp_value.toString()];
            pagesIndex++;
        }
    }
    return getLevelChilds(job);
}

async function getPages(job) {
    const fwiki = "https://" + categories[wikiIndex].wiki + ".wikipedia.org/wiki/";
    for (let t = 0; t < pages.length; t++) {
        try {
            await Article.create({ job_id: job.job_id, article: fwiki + pages[t][0], language: categories[wikiIndex].wiki, entity: pages[t][1], source: "category" });
        } catch (err) {
            console.log('Duplicate: ', fwiki + pages[t][0]);
        }
    }
    return wikiOpen(job);
}

function buildCategoryQuery(arr, start, end) {
    let j = start;
    let RQ = "";
    while (j < end && j < arr.length) {
        if (j > start)
            RQ += ',';
        RQ += "\"" + arr[j].replace(/\"/g,'\\"') + "\"";
        j++;
    }
    return `select page_namespace, page_title, pp_value from categorylinks, page, page_props where page_id = cl_from and page_id = pp_page and pp_propname = 'wikibase_item' and cl_to in (${RQ})`;
}

module.exports = async (job) => {
    console.log("Getting articles from category...");
    console.log("===========================================");
    await mappingStep(job);
    return Promise.resolve();
};
