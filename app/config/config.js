const fs = require('fs');

const config = JSON.parse(fs.readFileSync('./config/config.json'));
config.admin.realm = 'Admin area';
config.usersRealm = 'User area';

exports.admin = config.admin;
exports.limits = config.limits;
exports.users = config.users;
exports.usersRealm = config.usersRealm;
exports.databaseTunnels = config.databaseTunnels;
// exports.wmflabs = config.wmflabs;
