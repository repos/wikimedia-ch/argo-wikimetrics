require('dotenv').config({ path: '../.env' });

const { Sequelize, DataTypes } = require('sequelize');
const moment = require('moment');

const sequelize = new Sequelize(process.env.TARGET_DB, { logging: console.log });
function getDateType (name) {
  return {
    type: DataTypes.DATE,
    set (val) {
      if (typeof val !== 'date') {
        val = moment(val + '+0000', 'YYYYMMDDHHmmssZZ');
      }
      this.setDataValue(name, val);
    },
    get () {
      return moment(this.getDataValue(name)).format('DD/MM/YYYY');
    }
  };
}

const Job = sequelize.define('Job', {
  job_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  date: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.NOW,
    allowNull: false,
    get () {
      return moment(this.getDataValue('date')).format('DD/MM/YYYY');
    }
  },
  status: {
    type: DataTypes.STRING,
    defaultValue: 'pending',
    allowNull: false
  },
  config: {
    type: Sequelize.TEXT,
    allowNull: false,
    get () {
      const valstr = this.getDataValue('config');
      let val = null;
      try {
        val = JSON.parse(valstr || '[]');
      } catch (e) { }
      return val;
    },
    set (val) {
      if (typeof val !== 'string') {
        try {
          val = JSON.stringify(val);
        } catch (e) {
          val = null;
        }
      }
      this.setDataValue('config', val);
    }
  }
},
{
  tableName: 'jobs',
  timestamps: false
});

const Article = sequelize.define('Article', {
  article_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  job_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  article: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  language: {
    type: DataTypes.STRING,
    allowNull: false
  },
  entity: {
    type: DataTypes.STRING,
    allowNull: false
  },
  type: {
    type: DataTypes.STRING,
    allowNull: true
  },
  source: {
    type: DataTypes.STRING,
    allowNull: false
  }
},
{
  tableName: 'articles',
  timestamps: false,
  indexes: [
    {
      unique: true,
      fields: ['job_id', 'article']
    }
  ]
});

const User = sequelize.define('User', {
  user_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  job_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false
  },
  languages: DataTypes.INTEGER,
  last_white_edit: getDateType('last_white_edit'),
  first_all_edit: getDateType('first_all_edit')
},
{
  tableName: 'users',
  timestamps: false,
  indexes: [
    {
      unique: true,
      fields: ['job_id', 'username']
    }
  ]
});

const Counter = sequelize.define('Counter', {
  counter_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  job_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  type: {
    type: DataTypes.STRING,
    allowNull: false
  },
  language: {
    type: DataTypes.STRING,
    allowNull: false
  },
  value: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
},
{
  tableName: 'counters',
  timestamps: false
});

const Edit = sequelize.define('Edit', {
  edit_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  user_id: DataTypes.INTEGER,
  article_id: DataTypes.INTEGER,
  timestamp: getDateType('timestamp')
},
{
  tableName: 'edits',
  timestamps: false
});

Job.hasMany(Article, {
  foreignKey: {
    name: 'job_id'
  }
});

Job.hasMany(User, {
  foreignKey: {
    name: 'job_id'
  }
});

Job.hasMany(Counter, {
  foreignKey: {
    name: 'job_id'
  }
});

User.hasMany(Counter, {
  foreignKey: {
    name: 'user_id'
  }
});

User.hasMany(Edit, {
  foreignKey: {
    name: 'user_id'
  }
});

Counter.belongsTo(User, {
  foreignKey: {
    name: 'user_id'
  }
});

/** Added this to model for order by language **/
Counter.hasMany(Counter, {
  foreignKey: {
    name: 'user_id'
  }
});

Counter.belongsTo(Counter, {
  foreignKey: {
    name: 'user_id'
  }
});

Edit.belongsTo(User, {
  foreignKey: {
    name: 'user_id'
  }
});

Article.hasMany(Edit, {
  foreignKey: {
    name: 'article_id'
  }
});

Edit.belongsTo(Article, {
  foreignKey: {
    name: 'article_id'
  }
});

(async () => {
  if (require.main === module) {
    // Create the database
    await sequelize.sync();
  }
})();

module.exports = {
  Job,
  Article,
  User,
  Counter,
  Edit,
  sequelize
};
