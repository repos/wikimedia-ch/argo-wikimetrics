require('dotenv').config({ path: '../.env' });

const express = require('express');
const apicache = require('apicache').options({ debug: false }).middleware;
const morgan = require('morgan');

const app = express();

app.use(morgan('common'));
app.use(express.json());
const expressSwagger = require('express-swagger-generator')(app);

const options = {
  swaggerDefinition: {
    info: {
      description: 'This is a sample server',
      title: 'Swagger',
      version: '1.0.0'
    },
    host: 'localhost:8081',
    basePath: '/',
    produces: [
      'application/json'
    ],
    schemes: ['http'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: ''
      }
    }
  },
  basedir: __dirname, // app absolute path
  files: ['./api.js', './routes.js'] // Path to the API handle folder
};

expressSwagger(options);
require('./routes.js')(app, apicache);

const port = parseInt(process.env.SERVER_PORT);

const server = app.listen(port, 'localhost', function () {
  const host = server.address().address;
  const usesPort = server.address().port;
  console.log('Server listening at http://%s:%s', host, usesPort);
});
