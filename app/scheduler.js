const { Job } = require('./database');
const schedule = require('node-schedule');

const wikidata = require('./script/wikidata');
const category = require('./script/category');
const user = require('./script/user');
const statistic = require('./script/statistic');
const wikiner = require('./script/wikiner');

let lock = false;

schedule.scheduleJob('*/1 * * * *', async () => {
    if (lock == false) {
        lock = true;
    } else {
        return;
    }

    let job = await Job.findOne({
        where: {
            status: 'pending'
        },
        order: [
            ['date', 'ASC']
        ]
    });

    if (job == null) {
        // console.log('No job available');
        lock = false;
        return;
    }

    console.log('Starting job ' + job.name);

    try {
        await job.update({ status: 'running' });
        switch (job.config.startFromModule) {
            case 'wikidata':
                await tryWikidata(job)
                break;
            case 'category':
                await category(job);
                break;
            default:
                await tryWikidata(job);
                await category(job);
        }
        await user(job);
        await statistic(job);
        await wikiner(job);

        await job.update({ status: 'done' });
    } catch (e) {
        console.error(e);
        await job.update({ status: 'failed' });
    }

    lock = false;
});

(async () => {
    // Jobs cannot be running on startup
    let jobs = await Job.findAll({
        where: {
            status: 'running'
        }
    });

    for (let job of jobs) {
        await job.update({ status: 'failed' });
    }
})();

async function tryWikidata(job) {
    for (let i = 0; i < 3; i++) {
        try {
            await wikidata(job);
            return Promise.resolve();
        } catch (e) {
            console.error(e);
        }
        await sleep(5000);
    }
    return Promise.reject("Wikidata query failed");
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}