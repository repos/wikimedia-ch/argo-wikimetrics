let offset = 0;
let limit = 10;
let sort = 'date';
let sortType = 'desc';
let currentPage = 1;
let totalPages = 1;
let total = 0;

function loadJobs () {
  let url = '/api/jobs';
  url += handleSort(offset, limit, sort, sortType);
  const defaultOpts = () => {
    return {
      totalPages: totalPages,
      startPage: currentPage,
      hideOnlyOnePage: false,
      initiateStartPageClick: false,
      first: '&#12298;',
      prev: '〈',
      next: '〉',
      last: '&#12299',
      visiblePages: (totalPages > 5) ? 5 : totalPages,
      onPageClick: function (event, page) {
        offset = (page * limit) - limit;
        currentPage = page;
        loadJobs();
      }
    };
  };
  $.get('/views/templates/jobResultsTemplate.html', function (template) {
    $.ajax({
      type: 'GET',
      url: url,
      success: function (res) {
        total = res.count || 0;
        if (total > 0) {
          const output = Mustache.render(template, res.rows);
          $('#jobRes').html(output).fadeIn(2000);
          const paginationItem = $('#jobsPagination');
          if (paginationItem && paginationItem.twbsPagination) {
            paginationItem.twbsPagination('destroy');
            totalPages = Math.ceil(total / limit);
            console.log(totalPages, currentPage);
            paginationItem.twbsPagination(defaultOpts());
          }
        }
      }
    });
  });
}
function setSort (value) {
  sort = value.split('-')[0];
  sortType = value.split('-')[1];
  loadJobs();
}

function setItemsPerPage (value) {
  limit = value;
  currentPage = 1;
  offset = 0;
  loadJobs();
}
loadJobs();
