/** sort: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages */
const lingue = ['it', 'fr', 'en', 'als', 'de', 'rm'];
let sort = 'all_edits';
let sortType = 'desc';
let sortLanguage = 'total';
let total = 0;
let totalPages = 1;
let currentPage = 1;
let offset = 0;
let limit = 10;
let filterString = '';
let group = 'm';

function clearFilters () {
  $('#minAnzianità').val('');
  $('#minlast_white_edit').val('');
  $('#numberCreatedPages').val('');
  $('#numberCreatedPagesSelect').val('eq');
  $('#numberActiveProjects').val('');
  $('#numberActiveProjectsSelect').val('eq');
  $('#minEditNumber').val('');
  $('#minEditNumberSelect').val('eq');
  $('#minWhiteEditNumber').val('');
  $('#minWhiteEditNumberSelect').val('eq');
  lingue.forEach(lingua => $('#' + lingua + 'Lang').prop('checked', false));
}

function toggleFilters () {
  const filtersDiv = $('#filterContainers');
  if (filtersDiv[0].style.display !== 'none') {
    filtersDiv.hide();
  } else {
    filtersDiv.show();
  }
}

function downloadCsv () {
  const urlSplit = window.location.href.toString().split('/');
  const id = urlSplit[4];
  document.location.href = '/api/jobs/' + id + '/users?offset=0&limit=10000&sort=all_edits&sortType=desc&language=total&format=csv'
}

function loadJobUsers () {
  $('#noMoreItem').hide();
  const urlSplit = window.location.href.toString().split('/');
  const id = urlSplit[4];
  const sortString = handleSort(offset, limit, sort, sortType, sortLanguage);
  const sortFilter = sortString ? sortString + filterString : (filterString ? '?' + filterString : '');
  const url = '/api/jobs/' + id + '/users' + sortFilter;
  const paginationItem = $('#usersPagination');
  let template = '';
  const defaultOpts = () => {
    return {
      totalPages: totalPages,
      startPage: currentPage,
      hideOnlyOnePage: false,
      initiateStartPageClick: false,
      first: '&#12298;',
      prev: '〈',
      next: '〉',
      last: '&#12299',
      visiblePages: (totalPages > 5) ? 5 : totalPages,
      onPageClick: function (event, page) {
        offset = (page * limit) - limit;
        currentPage = page;
        loadJobUsers();
      }
    };
  };
  paginationItem.twbsPagination(defaultOpts());
  $.get('/views/templates/jobUsersTemplate.html')
    .done(
      temp => {
        template = temp;
        return $.ajax({
          type: 'GET',
          url: url
        }).done(res => {
          $('#sort_' + sort + '_' + sortType).addClass('selected');
          $('#filterContainers').hide();
          total = parseInt(res.total) || 0;
          if (total > 0) {
            $('#jobName').html(res.name + ' - ' + res.date);
            const objRender = {
              users: res.users,
              formatter: formatNumbers
            };
            const output = Mustache.render(template, objRender);
            $('#jobRes').html(output);
            paginationItem.twbsPagination('destroy');
            totalPages = Math.ceil(total / limit);
            paginationItem.twbsPagination(defaultOpts());
          }
        });
      });
  $('[data-toggle="tooltip"]').tooltip();
}

function setSort (sortSelected, itemDiv, language) {
  sort = sortSelected;
  sortLanguage = language || '';
  const item = $('#' + itemDiv.id);
  if (item.hasClass('selected')) {
    if (item.hasClass('asc')) {
      sortType = 'desc';
      item.removeClass('asc');
      item.addClass('desc');
    } else {
      sortType = 'asc';
      item.removeClass('desc');
      item.addClass('asc');
    }
  } else {
    $('.sortSelector').removeClass('selected');
    $('.sortSelector').removeClass('asc');
    $('.sortSelector').addClass('desc');
    item.addClass('selected');
    sortType = 'desc';
  }
  loadJobUsers();
}

function setItemsPerPage (value) {
  limit = value;
  currentPage = 1;
  offset = 0;
  loadJobUsers();
}

function loadJob () {
  const urlSplit = window.location.href.toString().split('/');
  const id = urlSplit[4];
  const statsUrl = '/api/jobs/' + id + '/stats?group=' + group + '&' + (filterString || '');
  const entityStatsUrl = '/api/jobs/' + id + '/typeStats?group=' + group + '&' + (filterString || '');
  lineChartDraw(statsUrl, 'jobGraph', 'language', group);
  lineChartDraw(entityStatsUrl, 'entityGraph', 'type', group);
}

function setGroup (groupBy) {
  group = groupBy;
  loadJob();
}

function filterJobUsers () {
  limit = 10;
  const seniorityInput = $('#minAnzianità');
  const lastEditInput = $('#minlast_white_edit');
  const numberCreatedPages = $('#numberCreatedPages');
  const numberCreatedPagesSelect = $('#numberCreatedPagesSelect');
  const numberActiveProjects = $('#numberActiveProjects');
  const numberActiveProjectsSelect = $('#numberActiveProjectsSelect');
  const minEditNumber = $('#minEditNumber');
  const minEditNumberSelect = $('#minEditNumberSelect');
  const minWhiteEditNumber = $('#minWhiteEditNumber');
  const minWhiteEditNumberSelect = $('#minWhiteEditNumberSelect');
  const languages = [];
  filterString = '';
  lingue.forEach(lingua => {
    if ($('#' + lingua + 'Lang').prop('checked')) {
      languages.push(lingua);
    }
  });
  if (languages.length > 0) {
    filterString += 'languages=' + languages.join(',') + '&';
  }
  if (seniorityInput.val()) {
    filterString += 'first_all_edit=' + seniorityInput.val() + '&';
  }
  if (lastEditInput.val()) {
    filterString += 'last_white_edit=' + lastEditInput.val() + '&';
  }
  if (numberActiveProjects.val() && numberActiveProjectsSelect.val()) {
    filterString += 'projects=' + numberActiveProjectsSelect.val() + ',' + numberActiveProjects.val() + '&';
  }
  if (minEditNumber.val() && minEditNumberSelect.val()) {
    filterString += 'all_edits=' + minEditNumberSelect.val() + ',' + minEditNumber.val() + '&';
  }
  if (minWhiteEditNumber.val() && minWhiteEditNumberSelect.val()) {
    filterString += 'white_edits=' + minWhiteEditNumberSelect.val() + ',' + minWhiteEditNumber.val() + '&';
  }
  if (numberCreatedPages.val() && numberCreatedPagesSelect.val()) {
    filterString += 'total_created_pages=' + numberCreatedPagesSelect.val() + ',' + numberCreatedPages.val() + '&';
  }

  loadJobUsers();
  loadJob();
}

loadJobUsers();
loadJob();
