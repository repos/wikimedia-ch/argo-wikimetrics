const lingue = ['it', 'fr', 'en', 'als', 'de', 'rm'];
const continueBy = ['A', 'B', 'C'];
let id;
let job = {
  config: {
    minEditNumber: 50,
    minEditSize: 1000,
    category: 'Q39',
    maxLevel: 4,
    languages: [],
    startFromModule: 'category',
    query: 'SELECT DISTINCT ?place ?placeLabel\n    WHERE {\n        wd:Q841386 wdt:P625 ?SJloc .\n        wd:Q512016 wdt:P625 ?SCloc .\n        SERVICE wikibase:box {\n            ?place wdt:P625 ?location .\n            bd:serviceParam wikibase:cornerSouthWest ?SJloc .\n            bd:serviceParam wikibase:cornerNorthEast ?SCloc .\n        }\n        SERVICE wikibase:label {\n            bd:serviceParam wikibase:language "en" .\n        }\n    }',
    oldestAcceptedEdit: '19000101000000',
    latestActivity: '20200101000000',
    blackPages: []
  }
};
function checkInputField (inputField) {
  if (inputField.val() && inputField.val() !== '') {
    inputField.removeClass('is-invalid');
    return true;
  } else {
    inputField.addClass('is-invalid');
    return false;
  }
}

function checkMaxLevel () {
  const maxLev = $('#maxLev');
  if (maxLev.val() && maxLev.val() !== '' && maxLev.val() <=   10) {
    maxLev.removeClass('is-invalid');
    $('#maxLevError').hide();
    return true;
  } else {
    maxLev.addClass('is-invalid');
    $('#maxLevError').show();
    return false;
  }
}
function isValid () {
  const nomeRicerca = $('#nameInput');
  const querySparql = $('#queryS');
  const categoriaInput = $('#categSelect');
  const maxLev = $('#maxLev');
  const lingueGroup = $('#languagesContainer');
  const lingueSingle = $('input[type="checkbox"]');
  const letterSingle = $('input[type="radio"]');
  let continueFromStep = '';
  const lang = [];
  if (!checkInputField(nomeRicerca)) {
    return false;
  }
  job.name = nomeRicerca.val();
  lingue.forEach(lingua => {
    if ($('#' + lingua + 'Lang').prop('checked')) {
      lang.push(lingua);
    }
  });
  continueBy.forEach(letter => {
    if ($('#' + letter + 'lineRadio').prop('checked')) {
      continueFromStep = letter;
    }
  });
  if (lang.length === 0) {
    lingueSingle.addClass('is-invalid');
    $('#lingueInvalid').show();
    return false;
  } else {
    lingueSingle.removeClass('is-invalid');
    lingueGroup.removeClass('is-invalid');
    $('#lingueInvalid').hide();
    job.config.languages = lang;
  }
  if (continueFromStep === '') {
    letterSingle.addClass('is-invalid');
    $('#letterInvalid').show();
    return false;
  } else {
    $('#letterInvalid').hide();
    letterSingle.removeClass('is-invalid');
  }
  const minEdit = $('#minEdit').val();
  const minEditSize = $('#minEditSize').val();
  const oldestAcceptedEdit = $('#oldestAcceptedEdit').val();
  const latestActivity = $('#latestActivity').val();
  if (minEdit) {
    job.config.minEditNumber = minEdit;
  }

  if (minEditSize) {
    job.config.minEditSize = minEditSize;
  }
  if (oldestAcceptedEdit) {
    job.config.oldestAcceptedEdit = oldestAcceptedEdit;
  }
  if (latestActivity) {
    job.config.latestActivity = latestActivity;
  }

  switch (continueFromStep) {
    case 'A': {
      if (checkInputField(querySparql)) {
        job.config.startFromModule = 'wikidata';
        job.config.query = querySparql.val();
        return true;
      }
      return false;
    }
    case 'B': {
      if (checkInputField(categoriaInput) && checkMaxLevel()) {
        job.config.startFromModule = 'category';
        job.config.category = categoriaInput.val();
        job.config.maxLevel = maxLev.val();
        return true;
      }
      return false;
    }
    case 'C': {
      if (checkInputField(querySparql) && checkInputField(categoriaInput) && checkMaxLevel()) {
        job.config.startFromModule = 'both';
        job.config.query = querySparql.val();
        job.config.category = categoriaInput.val();
        job.config.maxLevel = maxLev.val();
        return true;
      }
      return false;
    }
    default: {
      return false;
    }
  }
}

function createJob () {
  const searchBtn = $('#searchButton');
  const goToSearch = $('#goToSearch');
  const successMessage = $('#successMessage');
  const errorMessage = $('#errorMessage');
  if (isValid()) {
    searchBtn.prop('disabled', true);
    successMessage.hide();
    errorMessage.hide();
    goToSearch.hide();
    console.log(job)
    console.log(JSON.stringify(job))
    $.ajax({
      type: 'POST',
      url: '/api/jobs/',
      contentType: 'application/json',
      dataType: 'json',
      data: JSON.stringify(job),
      success: function () {
        goToSearch.show();
        successMessage.show();
      },
      error: function (err) {
        errorMessage.show();
        errorMessage.append(err.responseText);
        console.error(err);
      }
    }).always(
      () => $('#searchButton').prop('disabled', false)
    );
  }
  return false;
}

function editJob () {
  const goToSearch = $('#goToSearch');
  const successMessage = $('#successMessage');
  const errorMessage = $('#errorMessage');
  delete job.job_id;
  delete job.status;
  delete job.date;
  delete job.isEdit;
  console.log(job)
  console.log(JSON.stringify(job))
  $.ajax({
    type: 'POST',
    url: '/api/jobs/',
    contentType: 'application/json',
    dataType: 'json',
    data: JSON.stringify(job),
    success: function () {
      goToSearch.show();
      successMessage.show();
    },
    error: function (err) {
      errorMessage.show();
      errorMessage.append(err.responseText);
      console.error(err);
    }
  });
}

function selectCategoryStartModule(res) {
  if (res.config && res.config.languages) {
    res.config.languages.forEach(lang => {
      $('#' + lang + 'Lang').prop('checked', true);
    });
  }
  if (res.config && res.config.startFromModule) {
    const continueFromStep = res.config.startFromModule;
    switch (continueFromStep) {
      case 'wikidata': {
        $('#AlineRadio').prop('checked', true);
        break;
      }
      case 'category': {
        $('#BlineRadio').prop('checked', true);
        break;
      }
      case 'both': {
        $('#ClineRadio').prop('checked', true);
        break;
      }
      default: {
        console.log('missing start from module');
      }
    }
  }
  if (res.config && res.config.query){
    $('#queryS').val(res.config.query);
  }
}
function loadTemplate () {
  const urlSplit = window.location.href.toString().split('/');
  id = urlSplit[4];
  $.get('/views/templates/createJobTemplate.html', function (template) {
    if (id) {
      $.ajax({
        type: 'GET',
        url: '/api/jobs/' + id,
        success: function (res) {
          console.log(res);
          job = res;
          res.isEdit = true;
          const output = Mustache.render(template, res);
          $('#jobRes').html(output).fadeIn(2000);
          selectCategoryStartModule(res);
        }
      });
    } else {
      const res = {};
      res.isEdit = false;
      res.config = job.config;
      const output = Mustache.render(template, res);
      $('#jobRes').html(output).fadeIn(2000);
      selectCategoryStartModule(res);
    }
  });
}

loadTemplate();
