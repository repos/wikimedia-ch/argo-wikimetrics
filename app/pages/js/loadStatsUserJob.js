function loadJob (group) {
  const urlSplit = window.location.href.toString().split('/');
  const id = urlSplit[4];
  const userId = urlSplit[6];
  const statsUrl = '/api/jobs/' + id + '/stats/' + userId + '?group=' + group;
  const entityStatsUrl = '/api/jobs/' + id + '/typeStats/' + userId + '?group=' + group;
  lineChartDraw(statsUrl, 'userGraph', 'language', group);
  lineChartDraw(entityStatsUrl, 'userStatsGraph', 'type', group);
  $.get('/views/templates/statsUserTemplate.html', function (template) {
    $.ajax({
      type: 'GET',
      url: '/api/jobs/' + id + '/users/' + userId,
      success: function (res) {
        res.formatter = formatNumbers;
        const output = Mustache.render(template, res);
        $('#jobRes').html(output).fadeIn(2000);
        $('#usernameDiv')[0].innerHTML = res.username;
        $('#usernameDivBox')[0].innerHTML = res.username;
      }
    });
  });
  $('[data-toggle="tooltip"]').tooltip();
}

loadJob('m');
