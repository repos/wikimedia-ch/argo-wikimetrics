function handleSort (offset, limit, sort, sortType, sortLanguage) {
  let url = '';
  if (offset >= 0 || limit || sort || sortType || sortLanguage) {
    url += '?';
    if (offset >= 0) {
      url += 'offset=' + offset + '&';
    }
    if (limit) {
      url += 'limit=' + limit + '&';
    }
    if (sort) {
      url += 'sort=' + sort + '&';
    }
    if (sortType) {
      url += 'sortType=' + sortType + '&';
    }
    if (sortLanguage) {
      url += 'language=' + sortLanguage + '&';
    }
  }
  return url;
}
// Format numbers for mustache
const formatNumbers = function () {
  return function (text, render) {
    if (!render(text)) {
      return '0';
    }
    return render(text).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
  };
};
// Format numbers
const formatter = function (value) {
  return value.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
};
/**
 * D3 graph functions
 */
const groupbyFormat = {
  d: ['%Y-%m-%d', 'timeDay'],
  m: ['%Y-%m', 'timeMonth'],
  y: ['%Y', 'timeYear']
};

function chart (dataRaw, div, property, groupBy) {
  const languages = { it: 'Italian', fr: 'French', en: 'English', als: 'Alemannic', de: 'German', rm: 'Romansh' };
  const parseTime = d3.timeParse(groupbyFormat[groupBy][0]); // funzione per trasformare la stringa in data
  const formatDate = d3.timeFormat(groupbyFormat[groupBy][0]); // funzione per trasformare la data in stringa
  const chartSvg = $('#' + div + 'svg-graph' + div);
  const datesMap = {}; // Mappa dove ad ogni data in formato stringa è associato l'oggetto con le lingue
  let margin;
  let availH;
  const divEl = $('#' + div);
  const WINDOW_WIDTH = $(window).width();
  const entities = []; // Array con le entità (lingue o tipo di articoli) da usare come etichette nei grafici
  const datesG = []; // Array di date in formato stringa da usare come chiavi per la mappa

  divEl.html('');
  chartSvg.remove();

  const minDate = d3.min(dataRaw, d => parseTime(d.date));
  const maxDate = d3.max(dataRaw, d => parseTime(d.date));
  const offsetMinDate = d3[groupbyFormat[groupBy][1]].offset(minDate, -1); // Aggiunto una data al range perchè se no toglieva l'ultimo valore
  const offsetMaxDate = d3[groupbyFormat[groupBy][1]].offset(maxDate, 1); // Aggiunto una data al range perchè se no toglieva l'ultimo valore
  const allTicks = d3[groupbyFormat[groupBy][1]].every(1).range(offsetMinDate, offsetMaxDate); // Tutte le date necessarie al grafico nell'intervallo specificato
  allTicks.forEach(date => datesG.push(formatDate(date)));

  dataRaw.forEach(item => {
    if (!entities.includes(item[property])) { // propery corrisponde a language o type in base al tipo di grafico
      entities.push(item[property]);
    }
  });

  datesG.forEach(date => {
    if (!datesMap[date]) {
      datesMap[date] = {
        date: parseTime(date)
      };
      // setto tutti i valori a 0
      entities.forEach(entity => {
        datesMap[date][entity] = 0;
      });
    }
  });

  // FORMAT DATA
  dataRaw.forEach(item => {
    datesMap[item.date][item[property]] = +item.total_edits;
  });

  const data = Object.values(datesMap);
  const edits = entities.map(function (id) {
    return {
      id: id,
      values: data.map(d => { return { date: d.date, total_edits: d[id] }; })
    };
  });

  if (WINDOW_WIDTH < 576) {
    // smartphones
    availH = $(window).height() * 0.8;
    margin = { top: 10, right: 40, bottom: 40, left: 20 };
  } else {
    // tablets and desktop
    margin = { top: 10, right: 50, bottom: 40, left: 20 };
    availH = 600;
  }

  const width = Math.round(divEl.outerWidth()) - margin.left - margin.right;
  const height = availH - margin.top - margin.bottom;
  // Main SVG
  const svg = d3.select('#' + div)
    .append('svg')
    .attr('id', '#' + div + 'svg-graph')
    .style('position', 'relative')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  // SCALE OBJECTS
  const x = d3.scaleTime().range([0, width])
    .domain([d3.min(edits, d => d3.min(d.values, c => c.date)), d3.max(edits, d => d3.max(d.values, c => c.date))]);
  const x2 = d3.scaleTime().range([0, width]).domain([d3.min(edits, d => d3.min(d.values, c => c.date)), d3.max(edits, d => d3.max(d.values, c => c.date))]);
  const y = d3.scaleLinear().range([height, margin.top]).domain([0, d3.max(edits, d => d3.max(d.values, c => parseInt(c.total_edits)))]).nice();
  // const y2 = d3.scaleLinear().rangeRound([height, margin.top]).domain([0, d3.max(edits, d => d3.max(d.values, c => c.total_edits))]).nice();
  const z = d3.scaleOrdinal(d3.schemeCategory10);
  const xAxis = d3.axisBottom(x).tickFormat(d3.timeFormat(groupbyFormat[groupBy][0]));
  const yAxis = d3.axisLeft(y).scale(y);

  const valueline = d3.line().x(d => x(d.date)).y(d => y(d.total_edits || 0)).curve(d3.curveStepBefore);

  // Clip path (clip line outside axis)
  svg.append('defs')
    .append('svg:clipPath')
    .attr('id', 'clip')
    .append('svg:rect')
    .attr('id', 'clip-rect')
    .attr('width', width)
    .attr('height', height)
    .attr('x', 0)
    .attr('y', 0);

  const lineChart = svg.append('g')
    .attr('class', 'focus')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .attr('clip-path', 'url(#clip)');

  const focus = svg.append('g')
    .attr('class', 'focus')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  const gX = focus.append('g')
    .attr('class', 'x-axis')
    .attr('transform', 'translate(0,' + (height) + ')')
    .call(xAxis);

  const gY = focus.append('g')
    .attr('class', 'axis axis--y')
    .call(yAxis);

  // Line across the plots on mouse pointer
  const verticalLine = svg.append('g')
    .attr('class', 'focus')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .style('display', 'none');

  verticalLine.append('line').attr('class', 'lineHover')
    .style('stroke', '#999')
    .attr('stroke-width', 1)
    .style('shape-rendering', 'crispEdges')
    .style('opacity', 0.5)
    .attr('y1', -height)
    .attr('y2', 0);

  verticalLine.append('text').attr('class', 'lineHoverDate')
    .attr('text-anchor', 'middle')
    .attr('font-size', 12);

  function mousemove () {
    const x0 = x.invert(d3.mouse(this)[0]);
    const i = d3.bisectLeft(allTicks, x0);
    const d = data[i];

    verticalLine.select('.lineHover')
      .attr('transform', 'translate(' + x(d.date) + ',' + height + ')');

    verticalLine.select('.lineHoverDate')
      .attr('transform', 'translate(' + x(d.date) + ',' + (height + margin.top + 20) + ')')
      .text(formatDate(d.date));

    verticalLine.selectAll('.hoverCircle')
      .attr('cy', e => y(d[e]))
      .attr('cx', x(d.date));

    verticalLine.selectAll('.lineHoverText')
      .attr('transform', 'translate(' + (x(d.date)) + ',' + height / 2.5 + ')')
      .text(e => (languages[e] ? languages[e] : e) + ': ' + formatter(d[e]));

    if (x(d.date) > (width - width / 4)) {
      verticalLine.selectAll('text.lineHoverText').attr('text-anchor', 'end')
        .attr('dx', -10);
    } else {
      verticalLine.selectAll('text.lineHoverText')
        .attr('text-anchor', 'start')
        .attr('dx', 10);
    }
  }

  function update () {
    // Append data to main graph...

    gY.call(yAxis);

    const edit = lineChart.selectAll('.edits').data(edits);

    edit.exit().remove();

    edit.enter().insert('g', '.focus').append('path')
      .attr('class', 'line edits')
      .style('stroke', d => z(d.id))
      .merge(edit)
      .transition().duration(0)
      .attr('d', d => valueline(d.values));
  }

  // zoom behavior handler
  function zoomFunction () {
    if (d3.event.sourceEvent && d3.event.sourceEvent.type === 'brush') {
      return; // ignore zoom-by-brush
    }

    const t = d3.event.transform;
    x.domain(t.rescaleX(x2).domain());
    // lineChart.select('.line').attr('d', d => valueline(d.values));
    gX.call(xAxis);

    lineChart.selectAll('.line edits').remove();
    update();
  }

  // Zoom Function
  const zoom = d3.zoom()
    .scaleExtent([1, 50])
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .on('zoom', zoomFunction);

  // append zoom area
  svg.append('rect')
    .attr('class', 'zoom-area')
    .attr('width', width)
    .attr('height', height)
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .call(zoom);

  function tooltip () {
    const labelsG = verticalLine.selectAll('.lineHoverText').data(entities);

    labelsG.enter().append('text')
      .attr('class', 'lineHoverText')
      .style('fill', d => z(d))
      .attr('text-anchor', 'start')
      .attr('font-size', 12)
      .attr('dy', (param, i) => 1 + i * 2 + 'em')
      .merge(labelsG);

    const circles = verticalLine.selectAll('.hoverCircle').data(entities);

    circles.enter().append('circle')
      .attr('class', 'hoverCircle')
      .style('fill', d => z(d))
      .attr('r', 2.5)
      .merge(circles);

    svg.selectAll('.zoom-area')
      .on('mouseover', () => verticalLine.style('display', null))
      .on('mouseout', () => verticalLine.style('display', 'none'))
      .on('mousemove', mousemove);
  }

  tooltip();
  update();
}

const lineChartDraw = function (url, div, property, groupBy) {
  d3.json(url).then(data => chart(data, div, property, groupBy)).catch(err => console.error(err));
};
