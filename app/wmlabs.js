require('dotenv').config({ path: '../.env' });

const globalConfig = require('./config/config.js');

const mariadb = require('mariadb');

let connection;

const connect = async ( database ) => {

    // no database no party
    if( !database ) {
        // https://phabricator.wikimedia.org/T313737
        throw "Each connection should specify a database - otherwise I don't know which tunnel to use";
    }

    // always end previous connection
    if (connection && connection.isValid()) {
        await connection.end();
    }

    // database configuration (the port will be added later)
    let config =
    {
        host: 'localhost',
        database: database,
        user: process.env.WIKIMEDIA_DB_USER,
        password: process.env.WIKIMEDIA_DB_PASSWORD
    };

    /**
     * If you have the old configuration it will use a fixed port.
     *
     * If you have 'databaseTunnels', it will use the right port for
     * each database.
     *
     * See https://phabricator.wikimedia.org/T313737
     */
    if (globalConfig.databaseTunnels) {

        // note that the database ends with "_p"
        var languageTunnel = globalConfig.databaseTunnels[ database ];

        // no configuration
        if(!languageTunnel) {
            throw "Please add this database tunnel in your JSON config: " + database;
        }

        config.port = languageTunnel;
    } else {

        config.port = parseInt( process.env.WIKIMEDIA_DB_PORT );
    }

    connection = await mariadb.createConnection(config);

    return Promise.resolve();
}

const query = async (sql, database) => {
    if (!connection || !connection.isValid) {
        await connect( database );
    }

    try {
        try {
            result = await connection.query(sql);
        } catch (e) {
            console.error(e);

            // Try again
            await end();
            await connect( database );
            result = await connection.query(sql);
        }
    } catch (e) {
        console.error(e);
        return Promise.reject(e);
    }

    return Promise.resolve(result);
}

const end = async () => {
    if (connection) {
        await connection.end();
    }
    return Promise.resolve();
}

exports.connect = connect;
exports.query = query;
exports.end = end;
