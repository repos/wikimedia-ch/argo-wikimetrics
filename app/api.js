const { Job, Article, User, Edit, Counter, sequelize,langCounters,totatlCounters } = require('./database');
const { Sequelize, Op } = require('sequelize');
const moment = require('moment');
const stringify = require('csv-stringify')

// UTILS
/**
 * This function fix users array: transforms counters object in users properties and sorts the array
 * @param {array} users
 * @param {object} result
 * @param {object} res express response
 */
const fixUsersResults = function (users, result, res) {
  result.users = [];
  users.forEach(user => {
    const newUser = user.dataValues;
    newUser.last_white_edit = user.last_white_edit;
    newUser.first_all_edit = user.first_all_edit;
    if (user.Counters) {
      user.Counters.forEach(item => {
        const counter = item.dataValues;
        if (!newUser[counter.type]) {
          newUser[counter.type] = {};
        }
        newUser[counter.type][counter.language] = counter.value;
      });
      delete newUser.Counters;
      result.users.push(newUser);
    }
  });
  res.json(result);
};

/**
 * Function to obtain pagination and filters object from query string
 * @param {object} query
 * @returns {{pagination: {}, filters: {counter: [], users: {}}}}
 */
const parseFilters = function (query) {
  const filters = {
    counter: [],
    users: {}
  };
  const pagination = {};
  Object.keys(query).forEach(key => {
    switch (key) {
      case 'sort': case 'sortType': case 'limit': case 'offset': case 'language':
        pagination[key] = query[key];
        break;
      case 'first_all_edit': case 'last_white_edit':
        filters.users[key] = {
          [Op.gt]: moment(query[key] + '+0000', 'YYYYMMDDHHmmssZZ')
        };
        break;
      case 'projects':
        filters.users.languages = {
          [Op[query[key].split(',')[0]]]: query[key].split(',')[1]
        };
        break;
      case 'all_edits': case 'total_created_pages': case 'white_edits':
        filters.counter.push({
          type: key,
          language: 'total',
          value: {
            [Op[query[key].split(',')[0]]]: query[key].split(',')[1]
          }
        });
        break;
      case 'languages':
        query[key].split(',').forEach(lang => {
          filters.counter.push({
            type: 'all_edits',
            language: lang,
            value: {
              [Op.gte]: 1
            }
          });
        });
        break;
      default:
        break;
    }
  });

  return { filters: filters, pagination: pagination };
};

const orderIsByCounters = function (sort) {
  return sort === 'all_edits' || sort === 'white_edits' || sort === 'total_created_pages';
};

const orderIsByUsers = function (sort) {
  return sort === 'username' || sort === 'last_white_edit' || sort === 'first_all_edit' || sort === 'languages';
};

/**
 * Function to use correct sql dialect for format dates in query
 * @type {{sqlite: (function(*=, *): Fn), postgres: (function(*=, *): Fn)}}
 */
const timestampToStringFn = {
  sqlite: (colName, precision) => {
    const pattern = { y: '%Y', m: '%Y-%m', d: '%Y-%m-%d' };
    return Sequelize.fn('strftime', pattern[precision], Sequelize.col(colName));
  },
  postgres: (colName, precision) => {
    const pattern = { y: 'YYYY', m: 'YYYY-MM', d: 'YYYY-MM-DD' };
    return Sequelize.fn('to_char', Sequelize.col(colName), pattern[precision]);
  }
};

const havingCount = {
  sqlite: 'count(`Counter`.`user_id`) = ',
  postgres: 'count("Counter"."user_id") = '
};

/**
 * This function return an object with conditions for user query
 * @param {number} id job id
 * @param {object} pagination contains options for pagination
 * @param {string} pagination.sort could be one in: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages
 * @param {string} pagination.sortType could be asc or desc
 * @param {number} pagination.offset the page
 * @param {number} pagination.limit items for page
 * @param {object} filters on users properties
 * @param {boolean} includeCounters true if we want counters properties in users resulting
 * @returns {{offset: *, limit: number, where: {job_id: *}, order: [[string, string]]}}
 * offset and limit from pagination, where filters on  users properties, order [sort,sortType] from pagination
 */
const prepareUsersQuery = function (id, pagination, filters, includeCounters) {
  const order = orderIsByUsers(pagination.sort) ? pagination.sort : 'username';
  const orderType = orderIsByUsers(pagination.sort) ? pagination.sortType : 'ASC';
  const options = {
    where: {
      job_id: id
    },
    order: [
      [order, orderType]
    ],
    offset: pagination.offset,
    limit: pagination.limit
  };
  if (includeCounters) {
    options.include = [Counter];
  }
  if (filters && Object.keys(filters).length !== 0) {
    options.where = {
      [Op.and]: filters
    };
  }
  return options;
};

// QUERY

/**
 * Return a promise with a job by id
 * @param {number} id job id
 * @returns {Promise<>}
 */
const getJobById = function (id) {
  return Job.findOne({
    where: {
      job_id: id
    }
  });
};

/**
 * Return a promise with an array of users objects, sorted and filtered by user properties
 * @param {number} id
 * @param {object} pagination contains options for pagination
 * @param {string} pagination.sort could be one in: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages
 * @param {string} pagination.sortType could be asc or desc
 * @param {number} pagination.offset the page
 * @param {number} pagination.limit items for page
 * @param {object} filters users filters (optional)
 * @returns {Promise<>}
 */
const getUsersByJobId = async function (id, pagination, filters) {
  return User.findAll(prepareUsersQuery(id, pagination, filters, true));
};

/**
 * Return a promise with an user object
 * @param {number} id the user id
 * @returns {Promise} user object
 */
const getUserById = async function (id) {
  return User.findAll({
    where: {
      user_id: id
    },
    include: [Counter]
  });
};

/**
 * Return a promise, with the total number of users in the job corresponding to id
 * @param {number} id
 * @param {object} pagination contains options for pagination
 * @param {string} pagination.sort could be one in: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages
 * @param {string} pagination.sortType could be asc or desc
 * @param {number} pagination.offset the page
 * @param {number} pagination.limit items for page
 * @param {object} filters on users
 * @returns {number} the number of users matching filters
 */
const countUsersByJobId = async function (id, pagination, filters) {
  return User.count(prepareUsersQuery(id, pagination, filters, false));
};

/**
 * Return an array of users filtered by counters params
 * Filters users taking only ones that belong to job
 * Filters counters with object filters,
 * We are using OR operator because every user has many records on db each one with a counter,
 * than groups by userId and returns only the userId that match all filters, so that having count equal to filters length
 * @param {number} id job id
 * @param {object} filters counter filters
 * @param {[object]} filters.counter filters on counters
 * @param {object} filters.users filters on users
 * @returns [number]
 */
const getUsersIdFilteredByCounters = async function (id, filters) {
  const options = {
    attributes: ['user_id'],
    where: {
      job_id: id,
      [Op.or]: filters.counter
    },
    group: Sequelize.col('Counter.user_id'),
    having: Sequelize.literal(havingCount[sequelize.getDialect()] + filters.counter.length),
    raw: true
  };

  if (filters.users && Object.keys(filters.users).length !== 0) {
    options.include = [{
      model: User,
      attributes: [],
      where: {
        [Op.and]: filters.users
      },
      required: true
    }];
  }
  const usersFiltered = await Counter.findAll(options);
  const usersId = [];
  usersFiltered.forEach(user => {
    usersId.push(user.user_id);
  });
  return usersId;
};
/**
 * Returns an array of users ordered by counters params
 * Filters users, taking only ones that belong to users array and to job corresponding to id
 * orders by counter param specified in pagination.sort
 * returns an object with two properties:
 *  - total, the total number of users in the search,
 *  - users the users Id that belong to offset and limit
 * @param {[number]} users array with usersId on which filter
 * @param {number} id id of the job
 * @param {object} pagination contains options for pagination
 * @param {string} pagination.sort could be one in: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages
 * @param {string} pagination.sortType could be asc or desc
 * @param {number} pagination.offset the page
 * @param {number} pagination.limit items for page
 * @param {string} pagination.language the language on sort, default total
 * @param {object} filters counter filters
 * @param {object} filters.users object containing filters on user properties
 * @returns {Object}
 */
const getUsersIdOrderedByCounters = async function (users, id, pagination, filters) {
  const options = {
    attributes: ['user_id'],
    where: {
      [Op.and]: filters.users
    },
    include: [{
      model: Counter,
      attributes: ['user_id', ['value', 'totalAll']],
      required: true,
      where: {
        job_id: id,
        type: pagination.sort,
        language: 'total'
      },
      include: [{
        model: Counter,
        attributes: ['user_id', ['value', 'totalLang']],
        required: false,
        where: {
          job_id: id,
          type: pagination.sort,
          language: pagination.language
        },
        on: {'user_id': { [Op.eq]: sequelize.col("Counters.user_id") }}
      }]
    }],
    order: [
      [Sequelize.literal('"Counters->Counters"."value"'), pagination.sortType],
      [Sequelize.literal('"Counters"."value"'), pagination.sortType]
    ],
    offset: pagination.offset,
    limit: pagination.limit,
    subQuery: false,
    raw: true
  };

  if (users.length > 0) {
    options.where.user_id = users;
  }
  const usersRes = await User.findAndCountAll(options);
  const usersId = [];
  const total = usersRes.count;
  usersRes.rows.forEach(user => {
    usersId.push(user.user_id);
  });

  return {
    total: total,
    users: usersId
  };
};

/**
 * Return an array of users object, if order and other params are present apply them else the users order is the default
 * @param {number} usersId array of users to find
 * @param {object} pagination is optional, contains options for pagination
 * @param {string} pagination.sort could be one in: username,last_white_edit,first_all_edit, languages(numero progetti), all_edits, white_edits, total_created_pages
 * @param {string} pagination.sortType could be asc or desc
 * @param {number} pagination.offset the page
 * @param {number} pagination.limit items for page
 * @returns Promise
 */
const getUsersFilteredByCounter = async function (usersId, pagination) {
  const options = {
    where: {
      user_id: usersId
    },
    include: [Counter]
  };

  if (pagination) {
    options.order = [
      [pagination.sort, pagination.sortType]
    ];
    options.offset = pagination.offset;
    options.limit = pagination.limit;
  }

  return User.findAll(options);
};

// API
/**
 *Returns a job configuration
 * @route GET /api/jobs/{id}/
 * @param {number} id.path.required - job id
 * @returns {object} 200 - job configuration
 * @returns {Error}  404 - Job not found
 */
const getJob = function (req, res) {
  getJobById(req.params.id)
    .then((job) => {
      res.json(job);
    })
    .catch(err => {
      res.status(404).send(err);
    });
};

/**
 * Returns a promise with an array of entity and its total white edits number, for the specified user
 * @param {number} userId id of the user
 * @returns {Promise}
 */
const getUserArticles = function (userId) {
  return Edit.findAll({
    attributes: [
      [Sequelize.fn('count', Sequelize.col('*')), 'total_edits'],
      [Sequelize.col('type'), 'type']],
    where: {
      user_id: userId
    },
    include: [{
      model: Article,
      attributes: []
    }],
    group: [Sequelize.col('type')],
    order: [
      [Sequelize.col('type'), 'ASC']
    ]
  });
};
/**
 * Returns a user object, with counters and entities properties
 * @route GET /api/jobs/{id}/users/{userid}
 * @param {number} id.path.required - job id
 * @param {number} userid.path.required - user id
 * @returns {object} 200 - user object with entities
 * @returns {Error}  404 - Job not found
 *
 */
const getUser = function (req, res) {
  let newUser;
  getUserById(req.params.userid)
    .then(user => {
      newUser = user[0].dataValues;
      newUser.last_white_edit = user[0].last_white_edit;
      newUser.first_all_edit = user[0].first_all_edit;

      if (user[0].Counters) {
        user[0].Counters.forEach(item => {
          const counter = item.dataValues;
          if (!newUser[counter.type]) {
            newUser[counter.type] = {};
          }
          newUser[counter.type][counter.language] = counter.value;
        });
        delete newUser.Counters;
      }
    })
    .then(() => getUserArticles(req.params.userid))
    .then(result => {
      newUser.articles = result;
      res.json(newUser);
    })
    .catch(err => {
      console.dir(err);
      res.status(404).send(err);
    });
};

/**
 *Create a new job configuration
 * @route POST /api/jobs/
 * @returns {object} 201 - the job created
 * @returns {Error}  500 - generic error
 */
const createJob = function (req, res) {
  console.dir(req.body);
  Job.create(req.body)
    .then((jobCreated) => { res.status(201).send(jobCreated); })
    .catch(err => {
      console.dir(err);
      res.status(500).send(err);
    });
};

/**
 * Handler function for getJobUsers api, makes queries based on request parameters
 * @param {object} req express request
 * @param {object} res express response
 * @param {object} result temporary response object
 * @returns {Promise}
 */
const getUsers = async function (req, res, result) {
  let usersId = [];
  const filtersAndPagination = parseFilters(req.query);
  const pagination = filtersAndPagination.pagination;
  const filters = filtersAndPagination.filters;
  const jobId = req.params.id;
  pagination.sort = pagination.sort || 'username';
  pagination.sortType = pagination.sortType.toUpperCase() === 'DESC' ? 'DESC NULLS LAST' : 'ASC NULLS FIRST';
  pagination.limit = pagination.limit || '10';
  pagination.offset = pagination.offset || '10';
  // Vogliamo ottenere un export in formato csv
  if (req.query.format === 'csv') {
    const usersByJob = await getUsersByJobId(jobId, pagination, filters.users);
    result.total = await countUsersByJobId(jobId, pagination, filters.users);
    return fixUsersResults(usersByJob, result, {json: obj => {
      const csvHeader = ["Users", "Total edits", "IT", "FR", "EN", "DE", "RM", "ALS", "Total relevant edits", "IT", "FR", "EN", "DE", "RM", "ALS", "Seniority", "Active projects", "Total pages created", "Last edit"];
      const csvArray = [];
      obj.users.forEach(user => {
        try {
          const userArray = [user.username, user.all_edits.total, user.all_edits.it, user.all_edits.fr, user.all_edits.en, user.all_edits.de, user.all_edits.rm, user.all_edits.als, user.white_edits.total, user.white_edits.it, user.white_edits.fr, user.white_edits.en, user.white_edits.de, user.white_edits.rm, user.white_edits.als, user.first_all_edit, user.languages, user.total_created_pages.total, user.last_white_edit];
          csvArray.push(userArray);
        } catch (e) {
          console.log('Invalid user ' + user.user_id);
        }
      });
      const csvArraySorted = csvArray.sort((a, b) => b[1] - a[1]);
      csvArraySorted.unshift(csvHeader);
      stringify(csvArraySorted, (err, output) => {
        if (err) {
          return res.sendStatus(500);
        }
        res.contentType('text/csv');
        res.set('Content-Disposition', 'attachment; filename=export_job_' + jobId + '.csv');
        res.status(200).send(output);
      });
    }});
  }
  if ((!filters.counter || Object.keys(filters.counter).length === 0) && !orderIsByCounters(pagination.sort)) {
    /* Caso in cui non ci sia nè un filtro nè un ordinamento sui counters,
     fa una query semplice che restituisce gli utenti ordinati per username crescente,
     se non è specificato un ordinamento diverso sull'utente, e filtrati sempre sulle proprietà dell'utente
     */
    const usersByJob = await getUsersByJobId(jobId, pagination, filters.users);
    result.total = await countUsersByJobId(jobId, pagination, filters.users);
    return fixUsersResults(usersByJob, result, res);
  } else {
    /* Siamo nel caso in cui ci siano dei filtri sui contatori, e/o l'ordinamento
     Cerchiamo tutti gli userId degli utenti appartenenti a un job
    */
    if (filters.counter && Object.keys(filters.counter).length !== 0) {
      /* Ci sono dei filtri sui counters, quindi filtriamo gli usersId che abbiamo ottenuto in base a quelli */
      usersId = await getUsersIdFilteredByCounters(jobId, filters);
      if (usersId.length === 0) {
        result.users = [];
        result.total = 0;
        res.json(result);
        return result;
      } else {
        result.total = usersId.length;
      }
    }
    if (orderIsByCounters(pagination.sort)) {
      /* l'ordinamento è su uno dei counters, quindi prendo l'elenco degli userId ordinati e paginati */
      const usersObject = await getUsersIdOrderedByCounters(usersId, jobId, pagination, filters);
      const usersIdSorted = usersObject.users;
      const usersSorted = [];
      if (usersObject.users.length === 0) {
        result.users = [];
        result.total = 0;
        res.json(result);
        return result;
      } else {
        if (!result.total) {
          result.total = usersObject.total;
        }
      }

      /* dato l'elenco degli users id vado a prendere gli utenti corrispondenti */
      const users = await getUsersFilteredByCounter(usersObject.users);
      /* Correggo users array prima di restituirlo, inserendo i counters negli oggetti e ordinando l'array */
      usersIdSorted.forEach(id => {
        usersSorted.push(users.find(user => user.dataValues.user_id === id));
      });
      return fixUsersResults(usersSorted, result, res);
    } else {
      /* L'ordinamento è quello utente o non è specificato */
      if (!orderIsByUsers(pagination.sort)) {
        pagination.sort = 'username';
      }
      const users = await getUsersFilteredByCounter(usersId, pagination);
      return fixUsersResults(users, result, res);
    }
  }
};

/**
 *Returns an array with job users if order and other params are present apply them else the users order is the default'
 * @route GET /api/jobs/{id}/users
 * @param {number} id.path.required - job id
 * @param {number} limit.query - number of items for page default is 10
 * @param {number} offset.query - index of the item to start from default is 0
 * @param {string} sort.query - field on which sort (name or date) default is date
 * @param {string} sortType.query - type of sort ASC or DESC, default is DESC
 * @param {string} format.query - type of format json or csv, default is json
 * @returns {object} 200 - array of jobs
 * @returns {Error}  404 - Job not found
 * @returns {Error}  500 - Internal Server error
 */
const getJobUsers = function (req, res) {
  let result;
  if (req.params.id) {
    getJobById(req.params.id)
      .then(job => {
        result = job.dataValues;
        result.date = job.date;
        result.users = [];
        return getUsers(req, res, result);
      }, err => {
        res.status(404).send(err);
      })
      .catch(err => {
        console.error(err);
        res.status(500).send(err);
      });
  } else {
    res.status(400).send('Error: missing id');
  }
};

/**
 *Returns an array with jobs
 * @route GET /api/jobs
 * @param {number} limit.query - number of items for page default is 10
 * @param {number} offset.query - index of the item to start from default is 0
 * @param {string} sort.query - field on which sort (name or date) default is date
 * @param {string} sortType.query - type of sort ASC or DESC, default is DESC
 * @returns {object} 200 - array of jobs
 * @returns {Error}  400 - Unexpected error
 */
const getJobs = function (req, res) {
  const limit = req.query.limit ? req.query.limit : 10;
  const offset = req.query.offset ? req.query.offset : 0;
  const order = req.query.sort ? req.query.sort : 'date';
  const orderType = req.query.sortType ? req.query.sortType : 'DESC';
  Job.findAndCountAll({
    order: [
      [order, orderType]
    ],
    offset: offset,
    limit: limit
  })
    .then((jobs) => { res.json(jobs); })
    .catch(err => { res.status(400).send(err); });
};

const getWhereClauseForJobStats = async function (req) {
  const filters = parseFilters(req.query).filters;
  let usersId = [];
  let whereClause;
  if (Object.keys(filters.users).length === 0 && filters.counter.length === 0) {
    whereClause = {
      '$User.job_id$': req.params.id
    };
  } else {
    if ((!filters.counter || Object.keys(filters.counter).length === 0)) {
      usersId = await User.findAll({
        attributes: ['id'],
        where: {
          job_id: req.params.id,
          [Op.and]: filters
        }
      });
    } else {
      usersId = await getUsersIdFilteredByCounters(req.params.id, filters);
    }
    whereClause = {
      '$User.job_id$': req.params.id,
      '$User.user_id$': usersId
    };
  }
  return whereClause;
};

// edit per lingua nel tempo di un job
/**
 * Returns an array with job stats objects, total edits by language over time
 * @route GET /api/jobs/{id}/stats
 * @param {number} id.params.required - job id
 * @param {string} group.query - y = year,m = month, d=day the group by clause
 * @returns {object} 200 - array of edits objects,
 * @returns {Error}  404 - Job not found
 */
const getJobStats = async function (req, res) {
  let whereClause = {
    '$User.job_id$': req.params.id
  };

  try {
    whereClause = await getWhereClauseForJobStats(req);
    console.dir(whereClause);
  } catch (e) {
    console.dir(e);
  }
  Edit.findAll({
    attributes: [
      [Sequelize.fn('count', Sequelize.col('*')), 'total_edits'],
      [Sequelize.col('language'), 'language'],
      [timestampToStringFn[sequelize.getDialect()]('timestamp', req.query.group), 'date']],
    where: whereClause,
    include: [{
      model: User,
      attributes: []
    }, {
      model: Article,
      attributes: []
    }],
    group: ['date', Sequelize.col('language')],
    order: [
      [Sequelize.col('date'), 'ASC']
    ]
  })
    .then((edits) => {
      res.json(edits);
    })
    .catch(err => {
      console.dir(err);
      res.status(400).send(err);
    });
};

/**
 * Returns an array with job stats objects, total edits by entity(type) over time
 * @route GET /api/jobs/{id}/typeStats/
 * @param {number} id.path.required - job id
 * @param {string} group.query - y = year,m = month, d=day the group by clause
 * @returns {object} 200 - array of edits objects,
 * @returns {Error}  404 - Job not found
 */
const getJobTypeStats = async function (req, res) {
  try {
    const whereClause = await getWhereClauseForJobStats(req);
    Edit.findAll({
      attributes: [
        [Sequelize.fn('count', Sequelize.col('*')), 'total_edits'],
        [Sequelize.col('type'), 'type'],
        [timestampToStringFn[sequelize.getDialect()]('timestamp', req.query.group), 'date']],
      where: whereClause,
      include: [{
        model: User,
        attributes: []
      }, {
        model: Article,
        attributes: []
      }],
      group: ['date', Sequelize.col('type')],
      order: [
        [Sequelize.col('date'), 'ASC']
      ]
    })
      .then((edits) => {
        res.json(edits);
      })
      .catch(err => { res.status(400).send(err); });
  } catch (e) {
    console.dir(e);
  }
};

// edit per lingua nel tempo di un utente
/**
 *Returns an array with user stats white edits by language over time
 * @route GET /api/jobs/{id}/stats/{userid}
 * @param {number} id.path.required - job id
 * @param {number} userid.path.required - user id
 * @param {string} group.query - y = year,m = month, d=day the group by clause
 * @returns {object} 200 - array of edits objects,
 * @returns {Error}  404 - Job not found or user not found
 */
const getJobStatsUser = function (req, res) {
  Edit.findAll({
    attributes: [
      [Sequelize.fn('count', Sequelize.col('*')), 'total_edits'],
      [Sequelize.col('language'), 'language'],
      [timestampToStringFn[sequelize.getDialect()]('timestamp', req.query.group), 'date']],
    where: {
      user_id: req.params.userid
    },
    include: [{
      model: Article,
      attributes: []
    }],
    group: ['date', Sequelize.col('language')],
    order: [
      [Sequelize.col('date'), 'ASC']
    ]
  })
    .then((edits) => {
      res.json(edits);
    })
    .catch(err => { res.status(400).send(err); });
};

// edit per entità nel tempo di un utente
/**
 *Returns an array with user stats white edits by entity over time
 * @route GET /api/jobs/{id}/typeStats/{userid}
 * @param {number} id.path.required - job id
 * @param {number} userid.path.required - user id
 * @param {string} group.query - y = year,m = month, d=day the group by clause
 * @returns {object} 200 - array of edits objects,
 * @returns {Error}  404 - Job not found or user not found
 */
const getJobStatsUserType = function (req, res) {
  Edit.findAll({
    attributes: [
      [Sequelize.fn('count', Sequelize.col('*')), 'total_edits'],
      [Sequelize.col('type'), 'type'],
      [timestampToStringFn[sequelize.getDialect()]('timestamp', req.query.group), 'date']],
    where: {
      user_id: req.params.userid
    },
    include: [{
      model: Article,
      attributes: []
    }],
    group: ['date', Sequelize.col('type')],
    order: [
      [Sequelize.col('date'), 'ASC']
    ]
  })
    .then((edits) => {
      res.json(edits);
    })
    .catch(err => { res.status(400).send(err); });
};

exports.getJobs = getJobs;
exports.getJob = getJob;
exports.createJob = createJob;
exports.getJobUsers = getJobUsers;
exports.getUser = getUser;
exports.getJobStats = getJobStats;
exports.getJobTypeStats = getJobTypeStats;
exports.getJobStatsUserType = getJobStatsUserType;
exports.getJobStatsUser = getJobStatsUser;
